### 组织介绍
请描述组织的定位/愿景

## 项目框架地址

```
https://gitee.com/star_ring/product_architecture.git[跳转](https://gitee.com/star_ring/product_architecture.git)
```
详情说明[跳转](https://gitee.com/star_ring/nswag-studio-2nd-edition)
### 项目架构说明

+ 严格按照Ts来书写项目及组件，减少使用any定义
+ 字体为思源字体
  + SourceHanSansCN-Regular 正常字体
  + SourceHanSansCN-Medium 加粗字体
+ 代码压缩 compression-webpack-plugin
+ 配置文件 baseConfig.json 相关的基本配置数据
+ 接口工具 利用Nswage工具依据swagger.json生成
+ 工具地址[仓库地址](https://gitee.com/star_ring/nswag-studio-2nd-edition.git)
+ vue-kst-auth 封装的授权npm包
+ vue-router
+ vuex
+ element-ui
+ vue-property-decorator
+ axios

## 目录结构
```
|—— public
    |—— baseConfig.json // 接本配置
|—— src
    |—— assets
    |—— exports  // 导出web组件文件夹
    |—— plugins // 插件、接口等
    |—— router // 路由
    |—— store
        |—— baseData.ts // 基本数据
    |—— views
    | App.vue
```


### vue+ts组件结构

```
<template>
    <div class="index"></div>
</template>
<script lang="ts">
import { Component, Prop, Vue } from "vue-property-decorator";
import {
  InspectItemResultApi,
  RewriteResultDto,
  InspectItemResultDto,
  ImportanceLevel
} from "@/plugins/api";
import {
  AssetPropertyCustomeFieldApi,
  AssetCustomFieldDto
} from "@/plugins/AssetApi";
import { _axios } from "vue-kst-auth";
interface State {
  /** 数据状态 */
  loading: boolean;
}
@Component
export default class Index extends Vue {
  /** 组件传值 */
  /** 数据 */
  state: State = {
    loading: false
  };
  /** 生命周期 */
  /** 方法 */
  /** 监听 */
  /** 计算属性 */
}
</script>
<style lang="scss" scoped>
.index {}
</style>
```


### 正在使用的项目组件(web components)

+ [作业流程分组管理组件，使用查看wiki](https://gitee.com/star_ring/work_flow/wikis)
+ [资产模型项目,详情查看，wiki](https://gitee.com/star_ring/asset_model/wikis)
+ [公共数据，详情查看，wiki](https://gitee.com/star_ring/public_data/wikis)
+ [基础数据，实时数据，详情查看，wiki](https://gitee.com/star_ring/common_data/wikis)
+ [知识库，详情查看；wiki](https://gitee.com/star_ring/repository/wikis)
+ [作业审核，详情查看，wiki](https://gitee.com/star_ring/job_audit/wikis)
+ [审批，详情查看，wiki](https://gitee.com/star_ring/approval/wikis)
+ [消息中心，详情查看，wiki](https://gitee.com/star_ring/message-center/wikis)
+ [快速工单，详情查看，wiki](https://gitee.com/star_ring/work_order_system/wikis)
+ [自定义表单文档](https://mrhj.gitee.io/form-generator/#/)
### 代码片段

#### 浏览器导出PDF，打印

[详情查看wiki](https://gitee.com/star_ring/nswag-studio-2nd-edition/wikis/%E5%AF%BC%E5%87%BAPDF%EF%BC%8C%E4%BD%BF%E7%94%A8%E6%B5%8F%E8%A7%88%E5%99%A8%E8%87%AA%E5%B8%A6%E7%9A%84%E6%89%93%E5%8D%B0%E5%8A%9F%E8%83%BD?sort_id=4352928)

#### 登录组件、http请求错误提醒组件

```
<script>
   document.write(`<script type="text/javascript" src="/auth/components/auth.min.js?v=${Date.now()}""><\/script>`);
</script>

```
#### 阿里图标转换

1. 外网转发，内网依据转发下载css文件

```
<link rel="stylesheet" href="/aliIcon/t/font_1553133_dqtivxe1pvj.css">
```


#### 定时刷新执行


```
  /**
   * 定时执行
   * 入参是目标时间的小时数，取值0-23，当然可以根据需要拓展成分钟数，这里主要是提供思路所以从简
   * 初次进入的时候，计算当前时间和目标时间的距离，执行一次setTimeout(func,距离时间)，第二次开始，把setTimeOut的时候设置成24小时
   * targetHour: 小时，minutes： 分钟
   */
  setRegular(targetHour: number, minutes: number | undefined = 0) {
    /** 判断是否已超过今日目标小时，若超过，时间间隔设置为距离明天目标小时的距离 */
    let timeInterval = 0;
    /** 当前时间 */
    const nowTime = new Date();
    /** 计算当前时间的秒数 */
    let nowSeconds = 0;
    /** 计算目标时间对应的秒数 */
    let targetSeconds = 0;
    nowSeconds =
      nowTime.getHours() * 3600 +
      nowTime.getMinutes() * 60 +
      nowTime.getSeconds();
    targetSeconds = targetHour * 3600 + minutes * 60;
    timeInterval =
      targetSeconds > nowSeconds
        ? targetSeconds - nowSeconds
        : targetSeconds + 24 * 3600 - nowSeconds;
    setTimeout(this.updateDate, timeInterval * 1000);
  }
  /** 每天定时刷新日期更新数据 */
  updateDate() {
    this.reloadLayout();
    //之后每天调用一次
    setTimeout(this.updateDate, 24 * 3600 * 1000);
  }
```

#### 获取两个日期间，每天字符串


```
ts代码
/** 获取两个日期间，每天的日期 */
function formatEveryDay(start: any, end: any) {
  let dateList = [];
  var startTime = getDate(start);
  var endTime = getDate(end);

  while (endTime.getTime() - startTime.getTime() >= 0) {
    var year = startTime.getFullYear();
    var month =
      startTime.getMonth() + 1 < 10
        ? "0" + (startTime.getMonth() + 1)
        : startTime.getMonth() + 1;
    var day =
      startTime.getDate().toString().length == 1
        ? "0" + startTime.getDate()
        : startTime.getDate();
    dateList.push(year + "-" + month + "-" + day);
    startTime.setDate(startTime.getDate() + 1);
  }
  return dateList;
}

function getDate(datestr: any) {
  var temp = datestr.split("-");
  var date = new Date(temp[0], temp[1] - 1, temp[2]);
  return date;
}

export { formatEveryDay };
```


### 上传文件

引入npm包
```
import { _axios, baseConfig } from "vue-kst-auth";
```

```
    <el-upload
      class="upload-demo"
      :action="`/files/api/****`"
      :show-file-list="false"
      :headers="headers"
      :on-success="handleAvatarSuccess"
    >
      <el-button size="medium" type="primary">上传数据</el-button>
    </el-upload>
  /** 文件上传成功 */
  handleAvatarSuccess() {}
  /** 计算属性 */
  /** 获取headers */
  get headers() {
    return {
      __tenant: baseConfig.getCookie("__tenant"),
      Authorization: "Bearer " + baseConfig.getCookie("Abp.AuthToken")
    };
  }
```


#### js创建a便签，下载文件

```
/** 创建下载链接 */
const downloadHref = "";
 
/** 创建a标签并为其添加属性 */
let downloadLink = document.createElement("a");
downloadLink.href = downloadHref;
downloadLink.download = "资产与3D视图模板.xlsx";
/** 触发点击事件执行下载 */
downloadLink.click();
/** 下载完成进行释放 */
window.URL.revokeObjectURL(downloadHref);
```
### js通过token头部信息，获取文件流，下载文件

```
  /** 下载导入模板 */
  downloadfileTem() {
    var xhr = new XMLHttpRequest();
    var formData = new FormData();
    xhr.open("get", "/tai-zhang/api/asset-management/download-asset-template"); //url填写后台的接口地址，如果是post，在formData append参数（参考原文地址）
    xhr.setRequestHeader("__tenant", baseConfig.getCookie("__tenant"));
    xhr.setRequestHeader(
      "Authorization",
      "Bearer " + baseConfig.getCookie("Abp.AuthToken")
    );
    xhr.responseType = "blob";
    xhr.onload = function(e) {
      if (this.status == 200) {
        var blob = this.response;
        /** 创建下载链接 */
        // const downloadHref =
        //   "/tai-zhang/api/asset-management/download-asset-template";
        const downloadHref = window.URL.createObjectURL(blob);
        /** 创建a标签并为其添加属性 */
        let downloadLink = document.createElement("a");
        downloadLink.href = downloadHref;
        downloadLink.download = `台帐模板${Date.now()}.xlsx`;
        /** 触发点击事件执行下载 */
        downloadLink.click();
        /** 下载完成进行释放 */
        window.URL.revokeObjectURL(downloadHref);
      }
    };
    xhr.send(formData);
  }
```


### 动态引入文件

```
// 动态加载引用文件
    let files = this.$store.getters.getBaseData("files");
    let files_keys = Object.keys(files);
    let heatStr = document.getElementsByTagName("head")[0].innerHTML;
    files_keys.forEach((cur, index) => {
      let src = files[cur];
      if (src && /\.css$/.test(src.toString())) {
        heatStr += `<link rel="stylesheet" href="${src}?v=${Date.now()}" />`;
      }
      if (src && /\.ico$/.test(src.toString())) {
        heatStr += `<link rel="icon" href="${src}">`;
      }
    });
    document.getElementsByTagName("head")[0].innerHTML = heatStr;
```
### 获取二级域名

```

  /** 获取二级域名 */
  getOrginName() {
    /** 获取当前域名切割 */
    const hostnameList: string[] = window.location.hostname
      .split(".")
      .reverse();
    if (
      hostnameList.length > 2 &&
      this.state.kpiList.includes(hostnameList[2])
    ) {
      this.state.kpiName = hostnameList[2];
    } else {
      this.state.kpiName = "ebc";
    }
  }
```
### 动态引入组件

```
const resComponents: any = {};
const FormComponents = require.context(
  "@/components/FormList",
  false,
  /\.vue$/
);
FormComponents.keys().forEach(fileName => {
  const comp = FormComponents(fileName);
  resComponents[fileName.replace(/^\.\/(.*)\.\w+$/, "$1")] = comp.default;
});
```

```
@Component({
  components: {
    ...resComponents
  }
})
```



### 获取基础数据

```
document.write(
        `<script type="text/javascript" src="/common_data/components/common.min.js?v=${Date.now()}""><\/script>`
      );
<div is="common-datas" @commonDatas="commonDatas" @error="error"></div>
```

```
commonDatas(msg: any) {
    const [res] = msg.detail;
    /** 授权 */
    if (
      res.SessionState &&
      res.SessionState.auth &&
      res.SessionState.auth.grantedPolicies
    ) {
      this.$store.commit("setDataApi", {
        key: "grantedPolicies",
        value: res.SessionState.auth.grantedPolicies
      });
    }
    /** 部门人员数据 */
    if (res.organizationUsers) {
      this.$store.commit("setDataApi", {
        key: "organizationUsers",
        value: res.organizationUsers.items
      });
    }
    /** 部门数据 */
    if (res.organizationUnit) {
      this.$store.commit("setDataApi", {
        key: "organizationUnit",
        value: res.organizationUnit
      });
    }
    /** 人员数据 */
    if (res.users) {
      this.$store.commit("setDataApi", {
        key: "users",
        value: res.users.items
      });
    }
    /** 严重等级 */
    if (res.severityLevel) {
      this.$store.commit("setDataApi", {
        key: "severityLevel",
        value: res.severityLevel
      });
    }
    /** 基础数据 */
    if (res.baseData && res.baseData.items) {
      const data: any[] = res.baseData.items;
      // 资产分类
      const assetTypes = data.filter(val => val.category == "AssetCategory");
      this.$store.commit("setDataApi", {
        key: "assetTypes",
        value: assetTypes
      });
      // 位置
      const positions = data.filter(val => val.category == "positions");
      this.$store.commit("setDataApi", {
        key: "positions",
        value: positions
      });
      // 分组
      const grids = data.filter(val => val.category == "AlertGroup");
      this.$store.commit("setDataApi", {
        key: "grids",
        value: grids
      });
    }
  }
  error() {}
```

### 基础数据-运行模型数据下拉使用

```
<div is="common-asset-model" :code-id="" @change="getCommonData"></div>
/** 方法 */
/** 基础数据返回 */
  getCommonData(res: {
    [key: string]: any;
    detail: { [key: string]: any }[];
  }) {
  }
```
[详情wiki](https://gitee.com/star_ring/nswag-studio-2nd-edition/wikis/%E6%A8%A1%E5%9E%8B%E6%95%B0%E6%8D%AE%E4%BD%BF%E7%94%A8%EF%BC%8C%E8%BF%90%E8%A1%8C%E6%A8%A1%E5%9E%8B)

### 基础数据自定义高级查询

```
<div is="common-filter-data" json-key="" init-data="" default-layout="" @change="" @refresh=""></div>
示例：json-key="设备台账高级查询"
init-data: 查询初始化字符串，json-key不传情况下，页面有提醒
default-layout: 没有自定义数据情况下，默认布局字符串
```

### 基础数据展示
参数查看wiki[跳转](https://gitee.com/star_ring/nswag-studio-2nd-edition/wikis/%E5%9F%BA%E7%A1%80%E6%95%B0%E6%8D%AE%E5%B1%95%E7%A4%BA?sort_id=4082227)

### 基础数据下拉框使用
1. 引用基础组件js
2. 下拉组件使用

```
<div is="common-datas-info" @change="getCommonData"></div>
```
3.页面结果展示
![引用结果](https://images.gitee.com/uploads/images/2021/0601/164046_8d39d847_2064088.png "1622536709.png")

4.参数详情查看wiki[跳转](https://gitee.com/star_ring/nswag-studio-2nd-edition/wikis/%E5%9F%BA%E7%A1%80%E6%95%B0%E6%8D%AE%E4%B8%8B%E6%8B%89%E6%A1%86%E4%BD%BF%E7%94%A8?sort_id=4078964)

### 长链接

#### 使用封装好的组件

[设备实时数据组件](https://gitee.com/star_ring/nswag-studio-2nd-edition/wikis/pages)


#### 微软新版本长链接

```
import { HubConnectionBuilder, HubConnection } from "@microsoft/signalr";
/** 定时器 */
timer: HubConnection | undefined = undefined;

/** 实时数据 */
connection: undefined | any = undefined;
/** 资产标签实时数据 */
  initTagData() {
    // 注销实时数据
    this.connection?.stop();
    const tags = [];

    this.connection = new HubConnectionBuilder()
      .withUrl(
        `/asset-tag/signalr/realtime-data?group=data/realtime&heartbeat=1000&tags=${tags.join(
          ","
        )}&access_token=${baseConfig.getCookie("Abp.AuthToken")}`
      )
      .build();
    // 实时数据(资产标签)
    if (this.timer) {
      clearInterval(this.timer);
    }
    this.connection
      ?.start()
      .then(() => {})
      .catch(() => {
        this.signaleState();
      });
    this.connection?.on("GetRealtime", (res: ITagData) => {
      /** 资产标签实时数据缓存 */
      let tagDataList: ITagData[] =
        this.$store.getters.getBaseData("tagDataList") || [];
      // res.data = JSON.parse(res.data);
      /** 格式化新接收到的实时数据 */
      const body: ITagData = res;
      /** 缓存中是否有新来的数据 */
      // 历史数据（缓存）
      this.saveTagData(body);
      this.filterTagData(res);
    });
    // 长链接关闭操作
    this.connection?.onclose(() => {
      this.signaleState();
    });
  }
  /** 定时查看长连接状态 */
  signaleState() {
    this.timer = setInterval(() => {
      if (this.connection?.state != "Connected") {
        this.initTagData();
      }
    }, 1000 * 3);
  }
  /** 缓存实时数据, */
  saveTagData(data?: ITagData) {
    /** 资产标签实时数据缓存，历史数据 */
    let tagDataListHistory: ITagData[] =
      this.$store.getters.getBaseData("tagDataListHistory") || [];
    if (tagDataListHistory.length > 1000) {
      tagDataListHistory.shift();
    }
    if (data) {
      tagDataListHistory.push(data);
    }
    window.sessionStorage.setItem(
      "资产实时数据缓存",
      JSON.stringify(tagDataListHistory)
    );
    this.$store.commit("setBaseDataApi", {
      key: "tagDataListHistory",
      value: tagDataListHistory
    });
  }
  /** 获取临时缓存数据 */
  getTagData() {
    /** 资产标签实时数据缓存，历史数据 */
    let tagDataListHistory: ITagData[] = [];
    if (window.sessionStorage.getItem("资产实时数据缓存")) {
      tagDataListHistory = JSON.parse(
        window.sessionStorage.getItem("资产实时数据缓存") || "[]"
      );
    }
    this.$store.commit("setBaseDataApi", {
      key: "tagDataList",
      value: tagDataListHistory
    });
    this.state.factoryData.forEach(item => {
      if (!Number.isNaN(Number(item.value))) {
        item.value = this.NumberFormat(Number(item.value).toFixed(2));
      }
      /** 历史数据 */
      let historyData = tagDataListHistory.find(val => val.Key == item.tag);
      if (historyData) {
        if (Number.isNaN(Number(historyData.Value))) {
          item.value = historyData.Value;
        } else {
          item.value = this.NumberFormat(Number(historyData.Value).toFixed(2));
        }
      }
    });
  }
```
#### 微软老版本

```
1、引入相关js
    <script src="/kpi/browser/jquery.min.js"></script>
    <script src="/kpi/browser/jquery.signalR.min.js"></script>
2、
/** 实时数据 */
  connection = $.hubConnection(this.htjnYb, {
    useDefaultPath: false
  });
    // 注销实时数据
    this.connection.stop();
    this.connection.start().done(function() {
      console.log("链接成功");
    });
    var contosoChatHubProxy = this.connection.createHubProxy("MessagingHub");
    contosoChatHubProxy.on("receive", (res: ITagData) => {
        //接收数据
    })
    // this.$message.success("开始连接");
    // 实时数据(资产标签)
    this.connection
      .start({ transport: ["webSockets", "longPolling"] })
      .done((res: any) => {
        // this.$message.success("连接成功");
      });
```


### 如何加入
请发送申请邮件至

### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

### 联系
网站：
Follow @aaa on Weibo
邮箱:
